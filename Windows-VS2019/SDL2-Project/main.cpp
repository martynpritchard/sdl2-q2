/*
    Getting started with SDL2 - Just a Window. 
*/

//Include SDL library
#include "SDL.h"

//For exit()
#include <stdlib.h>

int main(int argc, char* args[])
{
    // Declare window and renderer objects
    SDL_Window* gameWindow = nullptr;
    SDL_Renderer* gameRenderer = nullptr;

    // Temporary surface used while loading the image
    SDL_Surface* temp = nullptr;
    // Texture which stores the actual sprite (this
    // will be optimised).
    SDL_Texture* texture;

    // SDL allows us to choose which SDL componets are going to be
// initialised. Lets go for everything for now!
    SDL_Init(SDL_INIT_EVERYTHING);
    gameWindow = SDL_CreateWindow("He...Goodbye World", // Window title
        SDL_WINDOWPOS_UNDEFINED, // X position
        SDL_WINDOWPOS_UNDEFINED, // Y position
        800, 600, // width, height
        SDL_WINDOW_SHOWN); // Window flags
        // Create our renderer
    gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

    //Setup sprite
    temp = SDL_LoadBMP("assets/images/test.bmp");
    // NOTE: SDL is limited to loading BMPs
    // We can solve this problem later.

    // Create a texture object from the loaded image
// - we need the renderer we are going to use to draw
// this as well!
// - this provides information about the target format
// to aid optimisation.
    texture = SDL_CreateTextureFromSurface(gameRenderer, temp);
    // Clean-up - we re done with our temporary image
    // now our texture has been created
    SDL_FreeSurface(temp);

    // 1. Clear the screen (black)
    SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
    // Colour provided as red, green, blue and alpha
    //(transparency) values (i.e. RGBA)
    SDL_RenderClear(gameRenderer);

    // 2. Draw the image
    SDL_RenderCopy(gameRenderer, texture, NULL, NULL);

    // 3. Present the current frame to the screen
    SDL_RenderPresent(gameRenderer);

    //Pause to allow the image to be seen
    SDL_Delay(10000);

    //Clean up!
    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(gameRenderer);
    SDL_DestroyWindow(gameWindow);
    //Shutdown SDL - clear up resources etc.
    SDL_Quit();
    // Exit the program
    exit(0);
}
